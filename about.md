---
layout: page
title: About
permalink: /about/
---

A reference for cryptocurrencies and blockchain in plain English.  Written by industry practitioners with a technical background.
