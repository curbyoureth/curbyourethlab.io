---
layout: post
title:  "What is the Bitcoin blockchain?"
date:   2018-05-15 22:53:55 +1000
categories: posts
---

The Bitcoin blockchain is just a **publicly visible** file which contains a **list of records**.  The list has a small number of special (yet simple) characteristics.

The most important special characteristics of this list are that the order of records can _never_ be altered, and a certain procedure has to be followed for a new line to be added to the file. [^1]

Many people will try to make Bitcoin and blockchains sound more complex than they really are, but at all times you just need to remember that the Bitcoin blockchain is _simply a public file containing a list of records_.

It can be helpful to substitute the word "blockchain" with the phrase "public list of records" to demystify a whole lot of what you read about Bitcoin and blockchain.  Take this [article at Harvard Business Review](https://hbr.org/2017/01/the-truth-about-blockchain):

The rather wordy and complex statement -

> Indeed, virtually everyone has heard the claim that **blockchain** will revolutionize business and redefine companies and economies

becomes - 

> Indeed, virtually everyone has heard the claim that **a public list of records** will revolutionize business and redefine companies and economies

See - much easier.

There are a large number of associated questions which you will likely have after reading the above, which will be answered in due course:

- What is Bitcoin and how does it relate to the Bitcoin blockchain?
- What does it mean to "own Bitcoins"?
- What are the "records" you're talking about above?
- What are the rules for adding records to the Bitcoin blockchain?
- How are records added to the Bitcoin blockchain?
- How do I obtain the "Bitcoin blockchain file"?

---


[^1]: For now you can take for granted how these characteristics actually work, they will be explained in due course.  At this point the most important part is recognising that the Bitcoin blockchain is just a list of records with some special rules