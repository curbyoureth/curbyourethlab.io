---
layout: post
title:  "Bitcoin for normal people"
date:   2018-05-24 22:53:55 +1000
categories: posts
---

## Introduction

### What is this guide?

Bitcoin and blockchain technologies have been in the news in increasing amounts recently.  The problem with many explainers and guides is that they often use unnecessarily complex ideas and don’t put these ideas in terms which can be understood by a regular person with no technical background.

This guide shows you how Bitcoin works and why you should care about it.  Rather than using abstract ideas it uses real world examples and focuses on the animal instinct things which you care about.  Who really cares about data on a computer - you want to be able to buy a Lamborghini, or stash away money.  I promise you you don’t need to know anything technical in advance.  The most technical thing you need to know before reading this is Excel.

Even though this guide doesn’t require any tech knowledge in advance, by the time you’re done you will be able to speak with the most technical people about the inner workings of Bitcoin.  They won’t be able to confuse you, and you will have some basic principles to which you can always bring back the conversation / fall back on.

### Who is this geared towards?

This guide is for regular people who take an interest in money but who don’t have a technical background.  You don’t have to have any knowledge of technology, maths, or anything of the sort.  It would be really helpful if you knew what a spreadsheet is :)

### What will you get?

Enough knowledge to be aware of how Bitcoin works, and why you might use it.  You’ll be able to have discussions with the most serious followers of Bitcoin as you’ll have a deep knowledge of the fundamentals.

### Why read this?

Everyone is talking about Bitcoin and cryptocurrencies right now, though I’d guess that very few (less than 1%) of people would have the tools to explain the most straightforward ideas like

* What does it mean to “own Bitcoin”?
* If I own Bitcoin, how might someone steal them from me?
* How do I know that Bitcoin is not a scam?
* Can a single person change Bitcoin?

These ideas could be explained by the common person on the street for regular types of money (balance at a bank, bank notes) however for various reasons there exist limited outlets which describe these ideas.  This guide will give you the fundamentals to make sound decisions to becoming involved in Bitcoin, and will allow you to follow on with current affairs related to Bitcoin, and to discuss Bitcoin with others and form your own viewpoints.

### How does the guide work?

This guide is broken up into 3 parts.  The first is a very brief introduction which contains a set of facts regarding Bitcoin.  These are the base level facts on which Bitcoin is built. It should only take you around 5 minutes to read this introduction.

There are a number of implementation details behind these facts, but if you read them and memorise them, you will be able to return to them when you’re building your mental model of how Bitcoin works.   If you read only 1 thing, read this introduction, as you will now have the knowledge of the base concepts behind Bitcoin.  While some of these facts may entail follow on questions (“If you type a number into the Phone app on an iPhone and then press the green button it will call that phone”), you can have a solid foundation on which to build your knowledge.

After reading this you will likely have many follow on questions.  These are answered in the explainer section 2.  This goes into detail in very simple and easy to follow terms on how the building blocks of Bitcoin (and the facts which were given in section 1) are actually implemented.  I promise that after reading this you will have an above average knowledge of some deeply technical concepts which are not understood by many (and if I’m wrong you can email me at [curbyourethereum@gmail.com](mailto:curbyourethereum@gmail.com) and book in a Calendly session for a 1 on 1 to go over the area we missed!).

The final section contains a set of real world examples of Bitcoin in our day to day lives and how to interpret these.  Want to know what Bobby Axelrod really meant when he gave $1m in crypto to Mafee in Showtime’s Billions?  Interested in how to interpret the “huge hack at Mt Gox in 2013”?  Using the basic pieces of knowledge we listed in section 1 and then explained in section 2, we’ll show you how this really works in section 3.

### Why was this guide written?
The author is involved in the tech industry.  I always wanted to have a guide which explained to people how Bitcoin worked in real terms, but didn’t use complex words or ideas.  To truly understand something is to be able to explain it to anyone - I don’t buy into the idea that some concepts are beyond explanation.  If you can’t explain something to a person without knowledge in that field then you don’t really understand it properly

## Section 1

Let’s start with some basic facts:

* There is a computer file called ***the Bitcoin blockchain (“the blockchain file”)***
* A ***duplicate blockchain file*** is distributed by a network of computers
* ***The blockchain file*** can be represented as a list of transactions in the form of an Excel spreadsheet as shown

![](/assets/explainer_excel.png)

* ***Bitcoin (abbreviated “BTC”)*** is the name given to the units in **Amount** column in ***the*** ***blockchain file***
* ***To own Bitcoins is to have the power to reassign Bitcoin amounts in the blockchain file from one Account ID to another Account ID***
* An ***account ID*** is a number randomly generated by the receiving party 
* When you ***reassign control*** of Bitcoins to another party, you have ***spent*** them
* When Bitcoins are ***spent*** then that transfer is shown on the blockchain file as a ***transaction***
* To ***spend*** Bitcoins you must have the ***unique secret PIN number*** which corresponds to the Account ID for those Bitcoins
* A ***unique secret PIN number*** is also known as a ***private key***
* If you lose the ***private key*** corresponding to specific Bitcoins in ***the blockchain file*** then they cannot be ***spent*** and so their value is now lost
* You can confirm that a transfer is complete by ensuring the ***transaction*** has been added to ***the blockchain file***
